using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using QuotesApi.Data;

namespace QuotesApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var webHost = CreateHostBuilder(args).Build();
            if (args.Length > 0 && args[0] == "migrate")
            {
                applyMigrations(webHost);
                Environment.Exit(0);
            }
            else
            {
                 webHost.Run();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });

        private static void applyMigrations(IHost webHost)
        {
            using (var context = (QuotesDbContext)webHost.Services.GetService(typeof(QuotesDbContext)))
            {
                if (context != null)
                {
                    context.Database.Migrate();
                }
                else
                {
                    Console.WriteLine("Db context is null");
                }
                
            }
            Console.WriteLine("Migrated");
        }
    }
}
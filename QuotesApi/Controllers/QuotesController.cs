﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuotesApi.Data;
using QuotesApi.Models;

namespace QuotesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuotesController : ControllerBase
    {

        private readonly QuotesDbContext _quotesDbContext;

        public QuotesController(QuotesDbContext quotesDbContext)
        {
            _quotesDbContext = quotesDbContext;
        }
          
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var quotes = await _quotesDbContext.Quotes.ToListAsync();
            
            return Ok(quotes);
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<Quote>>> Get(int id)
        {
            var quote = await _quotesDbContext.Quotes.FindAsync(id);
            
            if (quote == null)
                return NotFound();

            return Ok(quote);
        }

        [HttpPost]
        public async Task<ActionResult<Quote>> Post(Quote quote)
        {
            if (quote.Id != 0)
                return BadRequest();
            
            _quotesDbContext.Add(quote);
            await _quotesDbContext.SaveChangesAsync();

            return CreatedAtAction(nameof(Get), new {id = quote.Id}, quote);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] Quote quote)
        {
            if (id != quote.Id)
                return BadRequest();
            
            _quotesDbContext.Entry(quote).State = EntityState.Modified;
            await _quotesDbContext.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var quote = await _quotesDbContext.Quotes.FindAsync(id);
            if (quote == null)
                return NotFound();
            
            _quotesDbContext.Quotes.Remove(quote);
            await _quotesDbContext.SaveChangesAsync();
            
            return NoContent();
        }
        
    }


}
﻿FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
# copy csproj and restore as distinct layers
EXPOSE 80

WORKDIR /app
COPY QuotesApi/QuotesApi.csproj .
RUN dotnet restore

# copy everything else and build app
COPY QuotesApi/. .
RUN dotnet publish -c Release -o /app/publish

# final stage/image
FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build /app/publish .
ENTRYPOINT ["dotnet", "QuotesApi.dll"]
# For migration use docker exec quotesapi_quotes_1 dotnet QuotesApi.dll "migrate"
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  TuiButtonModule,
  TuiDropdownControllerModule,
  TuiExpandModule,
  TuiHostedDropdownModule,
  TuiRootModule,
  TuiSvgModule
} from "@taiga-ui/core";
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { QuoteListComponent } from './components/quote-list.component';
import { HttpClientModule } from "@angular/common/http";
import { QuoteComponent } from './components/quote.component';
import { TuiActionModule, TuiInputModule, TuiIslandModule, TuiTextAreaModule } from "@taiga-ui/kit";
import { ReactiveFormsModule } from "@angular/forms";
import { QuoteEditComponent } from './components/quote-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    QuoteListComponent,
    QuoteComponent,
    QuoteEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TuiRootModule,
    BrowserAnimationsModule,
    TuiActionModule,
    TuiIslandModule,
    TuiSvgModule,
    TuiExpandModule,
    TuiInputModule,
    ReactiveFormsModule,
    TuiTextAreaModule,
    TuiButtonModule,
    TuiHostedDropdownModule,
    TuiDropdownControllerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Injectable } from '@angular/core';
import { QuoteListApiService } from "./quote-list.api.service";
import { concat, Observable, ReplaySubject } from "rxjs";
import { Quote } from "../interfaces/quote";
import { map, toArray, withLatestFrom } from "rxjs/operators";

const defaults = [
  {
    id: 0,
    title: "Success",
    author: "Bernard Shaw",
    description: "Success does not consist in never making mistakes but in never making the same one a second time."
  },
  {
    id: 0,
    title: "Simplicity",
    author: "leonardo Da Vinci",
    description: "Simplicity is the ultimate sophistication"
  },
  {
    id: 0,
    title: "Forgiving",
    author: "Oscar Wilde",
    description: "Always forgive your enemies; nothing annoys them so much"
  },
  {
    id: 0,
    title: "Limitations",
    author: "Steve Jobs",
    description: "Your time is limited, so don’t waste it living someone else’s life"
  },
  {
    id: 0,
    title: "Music",
    author: "Henry Longfellow",
    description: "Music is the universal language of mankind"
  },
];

@Injectable({
  providedIn: 'root'
})
export class QuoteListService {

  private quoteListSubject = new ReplaySubject<Quote[]>(1);
  quoteList$ = this.quoteListSubject.pipe();

  constructor(private api: QuoteListApiService) { }

  loadQuoteList(): Observable<void> {
    return this.api.fetchList().pipe(
        map(quotes => {this.quoteListSubject.next(quotes); })
    )
  }

  edit(quote: Quote): Observable<void> {
    return this.api.put(quote).pipe(
        withLatestFrom(this.quoteList$),
        map(([, list]) => {
          const i = list.findIndex(({id}) => id === quote.id);
          const newList = [...list];

          newList.splice(i, 1, quote);
          this.quoteListSubject.next(newList);
        })
    )
  }

  remove(quote: Quote): Observable<void> {
    return this.api.remove(quote).pipe(
        withLatestFrom(this.quoteList$),
        map(([, list]) => {
          const i = list.findIndex(({id}) => id === quote.id);
          const newList = [...list];

          newList.splice(i, 1);
          this.quoteListSubject.next(newList);
        })
    )
  }

  add(quote: Quote): Observable<void> {
    return this.api.post(quote).pipe(
        withLatestFrom(this.quoteList$),
        map(([q, list]) => {
          this.quoteListSubject.next([...list, q])
        })
    )
  }

  addDefaults(): Observable<void> {
    const requests = defaults.map(quote => this.api.post(quote));
    return concat(...requests).pipe(
        toArray(),
        withLatestFrom(this.quoteList$),
        map(([newList, list]) => {
          if (!list) {
            list = [];
          }
          this.quoteListSubject.next([...list, ...newList]);
        })
    )
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { Quote } from "../interfaces/quote";
import { environment } from "../../environments/environment";



@Injectable({
  providedIn: 'root'
})
export class QuoteListApiService {

  constructor(private http: HttpClient) {}
  
  private path = `${environment.api}api/quotes`;

  fetchList(): Observable<Quote[]> {
    return this.http.get<Quote[]>(this.path);
  }

  put(quote: Quote): Observable<any> {
    return this.http.put(`${this.path}/${quote.id}`, quote);
  }
  
  post(quote: Quote): Observable<Quote> {
    return this.http.post<Quote>(this.path, quote);
  }

  remove(quote: Quote): Observable<any> {
    return this.http.delete(`${this.path}/${quote.id}`);
  }

  fetch(quote: Quote): Observable<Quote> {
    return this.http.get<Quote>(`${this.path}/${quote.id}`);
  }
}

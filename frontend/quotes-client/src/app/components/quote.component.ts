import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { Quote } from "../interfaces/quote";

@Component({
  selector: 'app-quote',
  template: `
    <tui-island [hoverable]="true">
      <div class="tui-island__content">
        <figure class="tui-island__figure">
          <tui-svg class="tui-space_left-3" src="tuiIconEditLarge" (click)="toggleEditing()"></tui-svg>
          <tui-svg class="tui-space_left-3" src="tuiIconRemoveLarge" (click)="handleRemove()"></tui-svg>
        </figure>
        <div style="width: 100%">
          <p class="tui-island__category">{{quote?.author}}</p>
          <h3 class="tui-island__title">{{quote?.title}}</h3>
          <p class="tui-island__paragraph">
            {{quote?.description}}
          </p>
          <tui-expand [expanded]="isEditing">
            <ng-template tuiExpandContent>
              <app-quote-edit [quote]="quote" (confirmEdit)="handleConfirmEdit($event)"></app-quote-edit>
            </ng-template>
          </tui-expand>
        </div>
      </div>
    </tui-island>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuoteComponent {
  @Input()
  quote: Quote | undefined;

  @Output()
  remove = new EventEmitter<Quote>();

  @Output()
  confirmEdit = new EventEmitter<Quote>();

  isEditing = false;

  toggleEditing(): void {
    this.isEditing = !this.isEditing;
  }

  handleRemove(): void {
    this.remove.emit(this.quote);
  }

  handleConfirmEdit(quote: Quote): void {
    this.confirmEdit.emit(quote);
  }
}

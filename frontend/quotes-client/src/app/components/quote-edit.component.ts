import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Quote } from "../interfaces/quote";
import { FormBuilder, FormGroup } from "@angular/forms";

@Component({
  selector: 'app-quote-edit',
  template: `
    <form class="tui-space_top-5" [formGroup]="form">
      <div class="tui-container tui-container_adaptive">
        <div class="tui-row tui-row_adaptive tui-space_top-5">
          <tui-input class="tui-col_4" formControlName="title">
            Title
          </tui-input>
          <tui-input class="tui-col_4" formControlName="author">
            Author
          </tui-input>
          <div class="tui-col_2">
            <button
                tuiButton
                appearance="secondary"
                type="button"
                (click)="handleConfirmEdit()"
            >
              Save
            </button>
          </div>
        </div>
        <div class="tui-row tui-row_adaptive tui-space_top-5">
          <div class="tui-col_8">
            <tui-text-area formControlName="description" [expandable]="true">
              Description
            </tui-text-area>
          </div>
          <div *ngIf="quote" class="tui-col_2">
            <button
                tuiButton
                appearance="outline"
                type="button"
                (click)="handleCancel()"
            >
              Cancel
            </button>
          </div>
        </div>
      </div>
    </form>
  `,
  styles: [
  ]
})
export class QuoteEditComponent implements OnChanges {

  constructor(private fb: FormBuilder) { }

  @Input()
  quote: Quote | undefined;

  @Output()
  confirmEdit = new EventEmitter<Quote>();

  ngOnChanges(simpleChanges: SimpleChanges): void {
    const quote = simpleChanges.quote;
    if (quote && quote.currentValue !== quote.previousValue) {
      const {title, description, author} = quote.currentValue as Quote;
      this.form.patchValue({ title, description, author});
    }
  }

  form = this.fb.group({
    title: '',
    description: '',
    author: ''
  })

  handleConfirmEdit(): void {
    if (this.quote) {
      this.confirmEdit.emit({ ...this.quote, ...this.form.value});
    } else {
      this.confirmEdit.emit({...this.form.value, id: 0});
    }
  }
  handleCancel(): void {
    if (this.quote) {
      const {title, description, author} = this.quote;
      this.form.patchValue({ title, description, author});
    }
  }

}

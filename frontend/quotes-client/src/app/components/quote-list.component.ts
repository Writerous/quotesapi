import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { QuoteListService } from "../services/quote-list.service";
import { Quote } from "../interfaces/quote";

@Component({
  selector: 'app-quote-list',
  template: `
    <div class="tui-container tui-container_adaptive">
      <div class="tui-row tui-row_adaptive tui-space_top-5">
        <div class="tui-col_12" style="display: flex; justify-content: space-between">
            <button
                tuiButton
                appearance="primary"
                type="button"
                (click)="open = !open"
            >
              Add new quote
            </button>
          <button
              tuiButton
              appearance="primary"
              type="button"
              (click)="handleAddDefaults()"
          >
            Add default quotes
          </button>
        </div>
      </div>
      <div class="tui-row tui-row_adaptive tui-space_top-5">
        <div class="tui-col_12">
          <tui-expand [expanded]="open">
            <ng-template tuiExpandContent>
              <tui-island class="tui-space_top-5">
                <app-quote-edit (confirmEdit)="handleAdd($event)"></app-quote-edit>
              </tui-island>
            </ng-template>
          </tui-expand>
        </div>
      </div>
      <ng-container *ngIf="(quoteList$ | async)?.length">
        <div class="tui-row tui-row_adaptive tui-space_top-5"  *ngFor="let quote of quoteList$ | async">
          <div class="tui-col_12">
            <app-quote [quote]="quote" (confirmEdit)="handleEdit($event)" (remove)="handleRemove($event)"></app-quote>
          </div>
        </div>
      </ng-container>
    </div>
    <ng-template #dropdown>
    </ng-template>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuoteListComponent implements OnInit {

  quoteList$ = this.quoteListService.quoteList$;
  open = false;
  constructor(private quoteListService: QuoteListService) { }

  ngOnInit(): void {
    this.quoteListService.loadQuoteList().subscribe();
  }

  handleEdit(quote: Quote): void {
    this.quoteListService.edit(quote).subscribe();
  }

  handleRemove(quote: Quote): void {
    this.quoteListService.remove(quote).subscribe();
  }

  handleAdd(quote: Quote): void {
    this.quoteListService.add(quote).subscribe();
  }

  handleAddDefaults(): void {
    this.quoteListService.addDefaults().subscribe();
  }

}

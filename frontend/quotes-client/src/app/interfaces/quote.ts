export interface Quote {
    id: number;
    title: string;
    author: string;
    description: string;
}
